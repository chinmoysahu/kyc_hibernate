package com.jspiders.kyc.repository;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import com.jspiders.kyc.entity.Document;
import com.jspiders.kyc.singleton.SingletonClass;

public class DocumentRepository {
	
	public void saveDocument(Document document) {
		
		try {
//			Configuration configuration = new Configuration();
//			configuration.configure();
			SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(document);
			transaction.commit();
			
//			SingletonClass.getInstanceOfSessionFactory();
			
		} catch (HibernateException e) {}
	}

	public Document findDocumentById(Long id) {
		
//		Configuration configuration = new Configuration();
//		configuration.configure();
		SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
		Session session = sessionFactory.openSession();
		Document document = session.get(Document.class, id);
		return document;
	}
	
	public void deleteDocumentById(Long id) {
		
//		Configuration configuration = new Configuration();
//		configuration.configure();
		SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		String hql = "delete from Document where id=:n";
		Query query = session.createQuery(hql);
		query.setParameter("n", id);
		int rowsDeleted = query.executeUpdate();
		transaction.commit();
		
		if(rowsDeleted>0) {
			System.out.println("Delete Operation Successful");
		}else {
			System.out.println("Delete Operation Failed");
		}
		
	}
	
	public void updateContactNumberById(String contactNumber, Long id) {
		
		Document document = findDocumentById(id);
		if(document!=null) {
			document.setContactNumber(contactNumber);
//			Configuration configuration = new Configuration();
//			configuration.configure();
			SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.merge(document);
			transaction.commit();
			System.out.println("Update Operation Successful");
		}else{
			System.out.println("Update Operation Failed");
		}
		
	}
	
	public void verifyDocumentById(Long id) {
		
		Document document = findDocumentById(id);
		if(document!=null) {
			document.setIsDocumentVerified(true);
//			Configuration configuration = new Configuration();
//			configuration.configure();
			SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.merge(document);
			transaction.commit();
			
			System.out.println("Document Verification Successful");
		}else {
			System.out.println("Document not verified");
		}
		
	}
	
	public List<Document> findAll(){
		
//		Configuration configuration = new Configuration();
//		configuration.configure();
		SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
		Session session = sessionFactory.openSession();
		String hql = "from Document";
		Query query = session.createQuery(hql);
		
		return query.list();
	}
	
	public List<Document> findAllDocumentByType(String documentType){
		
//		Configuration configuration = new Configuration();
//		configuration.configure();
		SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
		Session session = sessionFactory.openSession();
		
		String hql = "from Document where document_type=:dt";
		Query query = session.createQuery(hql);
		query.setParameter("dt", documentType);
		return query.list();
	}
	
//	public Document findByContactNumber(String contactNumber) {
//		
//		Configuration configuration = new Configuration();
//		configuration.configure();
//		SessionFactory sessionFactory = configuration.buildSessionFactory();
//		Session session = sessionFactory.openSession();
//		Transaction transaction = session.beginTransaction();
//		Document document = session.get(Document.class, contactNumber);
//		return document;
//	}
	
	public void updateCityAndPinCodeByContactNumber(String city, String pinCode,String contactNumber){
		
//		Document document = findByContactNumber(contactNumber);
//		if(document!=null) {
//			document.setCity(city);
//			document.setPinCode(pinCode);
//			Configuration configuration = new Configuration();
//			configuration.configure();
//			SessionFactory sessionFactory = configuration.buildSessionFactory();
//			Session session = sessionFactory.openSession();
//			Transaction transaction = session.beginTransaction();
//			session.merge(document);
//			transaction.commit();
//			System.out.println("Update Operation Successful");
//		}
		
//		Configuration configuration = new Configuration();
//		configuration.configure();
		SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		String hql = "update Document set city=:c,pinCode=:pc where contact_number=:cn";
		Query query = session.createQuery(hql);
		query.setParameter("c", city);
		query.setParameter("pc", pinCode);
		query.setParameter("cn", contactNumber);
		
		int rowsUpdated = query.executeUpdate();
		transaction.commit();
		
		if(rowsUpdated>0) {
			System.out.println("Update Operation Successful");
		}else {
			System.out.println("Update Operation Failed");
		}
		
	}
	
	
	public void deleteByFirstNameAndLastName(String firstName,String lastName){
		
//		Configuration configuration = new Configuration();
//		configuration.configure();
		SessionFactory sessionFactory = SingletonClass.getInstanceOfSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		String hql = "delete from Document where first_name=:fn and last_name=:ln";
		Query query = session.createQuery(hql);
		query.setParameter("fn", firstName);
		query.setParameter("ln", lastName);
		int rowsDeleted = query.executeUpdate();
		transaction.commit();
		
		if(rowsDeleted>0) {
			System.out.println("Delete Operation Successful");
		}else {
			System.out.println("Delete Operation Failed");
		}
	
	}
	
	
}
