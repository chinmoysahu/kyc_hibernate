package com.jspiders.kyc;

import java.util.List;
import com.jspiders.kyc.entity.Document;
import com.jspiders.kyc.repository.DocumentRepository;

public class App 
{
    public static void main( String[] args )
    {
    	Document document = new Document();
    	
    	document.setFirstName("Rahul");
    	document.setLastName("Malhotra");
    	document.setContactNumber("9438386638");
    	document.setCity("Bengaluru");
    	document.setState("Karnataka");
    	document.setCountry("India");
    	document.setPinCode("560037");
    	document.setIsDocumentVerified(false);
    	document.setDocumentType("Addhar Card");
    	DocumentRepository documentRepository = new DocumentRepository();
    	documentRepository.saveDocument(document);
    	
//    	Document findDocumentById = documentRepository.findDocumentById(3L);
//    	System.out.println(findDocumentById);
    	
//    	documentRepository.deleteDocumentById(6L);
    	
//    	documentRepository.updateContactNumberById("9438386638", 1L);
//    	documentRepository.updateContactNumberById("9583175020", 7L);
    	
//    	documentRepository.verifyDocumentById(4L);
    	
//    	List<Document> documentList = documentRepository.findAll();
//    	documentList.forEach(each->{
//    		System.out.println(each);
//    	});
    	
//    	List<Document> documentList = documentRepository.findAllDocumentByType("Passport");
//    	documentList.forEach(each->{
//    		System.out.println(each);
//    	});
    	
//    	documentRepository.updateCityAndPinCodeByContactNumber("Delhi", "110071", "9438386638");
    	
//    	documentRepository.deleteByFirstNameAndLastName("Raj", "Aryan");
//    	documentRepository.deleteByFirstNameAndLastName("Chinmoy", "Aryan");
    	
    	
    }
}
