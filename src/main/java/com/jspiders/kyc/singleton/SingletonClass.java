package com.jspiders.kyc.singleton;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class SingletonClass {
	
	private SingletonClass() {}
	private static SessionFactory factory = null;
	
	public static synchronized SessionFactory getInstanceOfSessionFactory(){
		
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
			
		if(factory == null)  factory = sessionFactory;
		
		return factory;
	}
}

