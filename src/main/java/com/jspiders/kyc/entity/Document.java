package com.jspiders.kyc.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import com.jspiders.kyc.constants.AppConstants;

@Entity
@Table(name=AppConstants.DOCUMENT_INFO)
public class Document implements Serializable {
	
	@Id
	@GenericGenerator(name="m_auto",strategy="increment")
	@GeneratedValue(generator="m_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="contact_number")
	private String contactNumber;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="country")
	private String country;
	
	@Column(name="pinCode")
	private String pinCode;
	
	@Column(name="is_document_verified")
	private Boolean isDocumentVerified;
	
	@Column(name="document_type")
	private String documentType;
	
	public Document() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Boolean getIsDocumentVerified() {
		return isDocumentVerified;
	}

	public void setIsDocumentVerified(Boolean isDocumentVerified) {
		this.isDocumentVerified = isDocumentVerified;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	@Override
	public String toString() {
		return "Document [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", contactNumber="
				+ contactNumber + ", city=" + city + ", state=" + state + ", country=" + country + ", pinCode="
				+ pinCode + ", isDocumentVerified=" + isDocumentVerified + ", documentType=" + documentType + "]";
	}

	
}
